# News Article Application

This is the News Article application that provides a REST API for consuming and creating resources.
The default settings for the application is to run on 

    localhost:8080


## Build the Application:
The application uses maven to build
Do a [Maven](https://maven.apache.org/) install i.e.

    mvn clean install

A jar file should be generated in target folder.

## Getting Started
### Docker Environment
The application is based on spring boot and uses embedded tomcat.
To run the application on docker,use the main method given in

    src/main/java/com.company/NewsArticleApplication.java

For further information use the following link:

    [Running on Docker] https://docs.spring.io/spring-boot/docs/current/reference/html/deployment.html

### *NIX Environment
To run the application on linux, use the following link. 

    [Running on *NIX] https://docs.spring.io/spring-boot/docs/current/reference/html/deployment.html#deployment.installing.nix-services
