package com.company.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Hot implements Relevance{

    private LocalDateTime creationTime;

    private Integer fullStopCount;

    private Integer exclamationMarkCount;


    @Override
    public void relevanceCriteria(String Text, LocalDateTime localDateTime) {

    }
}
