package com.company.model;

import java.time.LocalDateTime;

public interface Relevance {

     public void relevanceCriteria(String Text, LocalDateTime localDateTime);
}
