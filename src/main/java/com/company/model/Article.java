package com.company.model;

import java.time.LocalDateTime;

/**
 * The article interface
 */
public interface Article {

    /**
     * Method to get the title of an article
     * @return title of the article
     */
    public String getTitle();

    /**
     * Methd to get the creation date of the article
     * @return creation date of the article
     */
    public LocalDateTime getCreationDate();

}
