package com.company.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Boring  implements Relevance{

    private LocalDateTime creationTime;

    private Integer fullStopCount;

    private Integer commaCount;

    @Override
    public void relevanceCriteria(String Text, LocalDateTime localDateTime){

    }
}
