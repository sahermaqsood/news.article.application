package com.company.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;


import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Model class for News Article
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
public class NewsArticle implements Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String text;

    private String title;

    @CreationTimestamp
    private LocalDateTime creationDate;

    /**
     * Single argument constructor
     * @param title title of the article
     */
    public NewsArticle (String title){
        this.title = title;
    }

    /**
     * Two argument constructor
     * @param title title of the article
     * @param text text of the article
     */
    public NewsArticle (String title, String text ){
        this.title = title;
        this.text = text;
    }
}
