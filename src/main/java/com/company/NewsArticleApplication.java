package com.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class that boots/starts the application
 */
@SpringBootApplication
public class NewsArticleApplication {

    public static void main (String [] args) {
        SpringApplication.run(NewsArticleApplication.class, args);
        System.out.println("application started!");
    }
}