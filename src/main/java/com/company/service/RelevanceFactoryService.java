package com.company.service;

import com.company.model.Boring;
import com.company.model.Hot;
import com.company.model.Relevance;
import com.company.model.Standard;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@Service
public class RelevanceFactoryService {

    public Relevance getRelevance(String text, LocalDateTime creationDateTime){

        if((text == null || text.trim().isEmpty())  ||
                (creationDateTime == null || creationDateTime.toString().trim().isEmpty())){
            return null;
        }

        Integer exclamationMarkCount = StringUtils.countOccurrencesOf(text, "!");
        Integer fullStopCount = StringUtils.countOccurrencesOf(text, ".");
        Integer commaCount = StringUtils.countOccurrencesOf(text, ",");

        if(creationDateTime.isAfter(LocalDateTime.now().minusMinutes(1)) &&
                exclamationMarkCount > fullStopCount) {
            return new Hot();
        }

        if(creationDateTime.isAfter(LocalDateTime.now().minusMinutes(5)) &&
            commaCount > fullStopCount){
            return new Boring();
        }

        else return new Standard();

    }
}
