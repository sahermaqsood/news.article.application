package com.company.service;

import com.company.model.NewsArticle;
import com.company.repository.NewsArticleRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service class for News Article
 */
@Getter
@Setter
@Service
public class NewsArticleService {

    @Autowired
    NewsArticleRepository newsArticleRepository;

    private List<NewsArticle> newsArticleList = new ArrayList<>();

    /**
     * Method to return all {@link NewsArticle} objects
     * @return list of {@link NewsArticle}
     */
    public List<NewsArticle> getAllNewsArticles(){
        List<NewsArticle> newsArticleList = new ArrayList<>();
        newsArticleRepository.findAll().forEach(newsArticleList::add);
        return newsArticleList;
    }

    /**
     * Get a single {@link NewsArticle} based on the id
     * @param id id of the News article
     * @return ResponseEntity
     */
    public ResponseEntity getNewsArticle(Integer id){
        Optional<NewsArticle> newsArticleFound = newsArticleRepository.findById(id);
        if(newsArticleFound.isPresent()){
            return ResponseEntity.ok(newsArticleFound);
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).
                    body("News artcle with given id does not exist.");
        }
    }

    /**
     * Method to create new {@link NewsArticle} object
     * @param newsArticle Newly created object
     * @return ResponseEntity
     */
    public ResponseEntity createNewsArticle(NewsArticle newsArticle) {
        try{
            NewsArticle createdNewsArticle = new NewsArticle(newsArticle.getTitle(), newsArticle.getText());
            newsArticleRepository.save(createdNewsArticle);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(createdNewsArticle.getId())
                    .toUri();

            return ResponseEntity.created(uri)
                    .body(createdNewsArticle);
        } catch (Exception exception){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
                    body("Something went wrong.Please try again later.");
        }
    }

    /**
     * Method to update {@link NewsArticle} object
     * @param newsArticleUpdated Updated {@link NewsArticle}
     * @param id id of the {@link NewsArticle} to be updated
     * @return ResponseEntity
     */
    public ResponseEntity updateNewsArticle(NewsArticle newsArticleUpdated, Integer id) {
        NewsArticle newsArticleMatched;
        List <NewsArticle> newsArticleList = getAllNewsArticles();
        //Assuming PUT REST call only updates and does not create a news resource if not present.
        for(int i =0; i< newsArticleList.size(); i++){
            newsArticleMatched = newsArticleList.get(i);
            if (newsArticleMatched.getId().equals(id)){
                //Since it is an update, assuming the creation date and id remains same.
                newsArticleUpdated.setCreationDate(newsArticleMatched.getCreationDate());
                newsArticleUpdated.setId(newsArticleMatched.getId());
                newsArticleRepository.save(newsArticleUpdated);
                return ResponseEntity.ok(newsArticleUpdated);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).
                body("News article with given id does not exists.");
    }
}
