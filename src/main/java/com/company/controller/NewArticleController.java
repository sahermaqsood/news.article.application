package com.company.controller;

import com.company.model.NewsArticle;
import com.company.service.NewsArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Rest Controller for {@link NewsArticle} resource
 */
@RestController
@RequestMapping(path="/NewsArticle")
public class NewArticleController {

    @Autowired
    private NewsArticleService newsArticleService;

    /**
     * Method to get all {@link NewsArticle} resources
     * @return list of all {@link NewsArticle} resources
     */
    @RequestMapping(path= "/fetchAll", method = RequestMethod.GET)
    public List<NewsArticle> getAllNewsArticles(){
        return newsArticleService.getAllNewsArticles();
    }

    /**
     * Method to get one {@link NewsArticle} resource
     * @param id - id of the resource to get
     * @return ResponseEntity object
     */
    @RequestMapping(path= "fetchOne/{id}", method = RequestMethod.GET)
    public ResponseEntity getOneNewsArticle(@PathVariable Integer id) {
        return newsArticleService.getNewsArticle(id);
    }

    /**
     * Method to create new/POST {@link NewsArticle} resource
     * @param newsArticle with at least {@link NewsArticle#getText()} field
     * @return ResponseEntity
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity createNewsArticle(@RequestBody NewsArticle newsArticle){
        if(newsArticle.getTitle() == null || newsArticle.getTitle().trim().isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).
                    body("Article must have a title");

        }else{
            return newsArticleService.createNewsArticle(newsArticle);
        }
    }

    /**
     * Method to update/PUT a {@link NewsArticle} resource
     * @param newsArticle the updated resource
     * @param id id of the resource to be updated
     * @return ResponseEntity
     */
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateNewsArticle(@RequestBody NewsArticle newsArticle, @PathVariable Integer id){
        //TODO: should creation date be updated?
        if(newsArticle.getTitle() == null || newsArticle.getTitle().trim().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).
                    body("Article must have a title.");
        } else{
            return newsArticleService.updateNewsArticle(newsArticle,id);
        }
    }
}
