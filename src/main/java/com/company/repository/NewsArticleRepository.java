package com.company.repository;

import com.company.model.NewsArticle;
import org.springframework.data.repository.CrudRepository;

public interface NewsArticleRepository extends CrudRepository<NewsArticle, Integer> {
}
