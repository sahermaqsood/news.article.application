package com.company;

import com.company.service.RelevanceFactoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Test class for relevance service factory
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RelevanceServiceFactoryTest {

    @Autowired
    private RelevanceFactoryService relevanceFactoryService;

    /**
     * Test {@link RelevanceFactoryService#getRelevance(String, LocalDateTime)} with
     * null or empty input
     */
    @Test
    public void getRelevance_nullOrEmptyTest(){
        assertNull(relevanceFactoryService.getRelevance(null, null));
        assertNull(relevanceFactoryService.getRelevance("", null));
        assertNull(relevanceFactoryService.getRelevance(null, LocalDateTime.now()));
        assertNull(relevanceFactoryService.getRelevance("", LocalDateTime.now()));
    }
}
